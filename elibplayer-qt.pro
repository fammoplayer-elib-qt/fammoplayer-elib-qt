#-------------------------------------------------
#
# Project created by QtCreator 2013-06-23T11:40:43
#
#-------------------------------------------------

QT       += core multimedia

QT       -= gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = elibplayer-qt
CONFIG   += console c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    player.cpp \
    queue.cpp \
    library.cpp \
    input.cpp \
    mainwindow.cpp

HEADERS += \
    player.h \
    queue.h \
    library.h \
    input.h \
    mainwindow.h

FORMS    += mainwindow.ui

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libevdev
