#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "player.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updatePlaylist();
    void updatePlaying(Player::State state);

signals:
    void on_playPause_clicked();
    void on_prevTrack_clicked();
    void on_prevBook_clicked();
    void on_nextTrack_clicked();
    void on_nextBook_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
