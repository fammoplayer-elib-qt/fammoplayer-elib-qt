#include "input.h"

#include <QDir>
#include <QDebug>
#include <QThread>

class Thread : public QThread {
public:
  Input *i;     
protected:
  void run() {
    while (true)
      i->handleKeypress();
  }
};

Input::Input(QString path, QObject *parent) :
  QObject(parent),
  m_dev(path),
  m_installedTimer(nullptr)
{
  m_dev.open(QFile::ReadOnly);
  libevdev_new_from_fd(m_dev.handle(), &m_evdev);
  Thread *t = new Thread();
  connect(this, &Input::rawKeyCode, this, &Input::handleRawKeyCode);
  t->i = this;
  t->start();
}

void Input::handleKeypress()
{
  struct input_event ev;
  int rc = libevdev_next_event(m_evdev, LIBEVDEV_READ_FLAG_NORMAL | LIBEVDEV_READ_FLAG_BLOCKING, &ev);
  if (rc == 0) {
    printf("Event: %s %s %d\n",
           libevdev_event_type_get_name(ev.type),
           libevdev_event_code_get_name(ev.type, ev.code),
           ev.value);
    /* Need to switch back to main thread. Do it through a signal */
    if (ev.type == EV_KEY)
      emit rawKeyCode(ev.type, ev.code, ev.value);
  }
}

void Input::handleRawKeyCode(int type, int code, int value)
{
  if (!m_installedTimer && value == 1) {
    m_installedTimer = new QTimer(this);
    m_current_type = type;
    m_current_code = code;
    connect(m_installedTimer, &QTimer::timeout, this, &Input::timerExpired);
    m_installedTimer->setSingleShot(true);
    m_installedTimer->start(1000);
  } else if (m_installedTimer && type == m_current_type
             && code == m_current_code && value == 0) {
    m_installedTimer->stop();
    delete m_installedTimer;
    m_installedTimer = nullptr;
  }
  if (type != EV_KEY || value != 0)
    return;
  switch (code) {
    case KEY_B: emit keyB(); break;
    case KEY_F: emit keyF(); break;
    case KEY_P: emit keyP(); break;
    case KEY_N: emit keyN(); break;
    case KEY_SPACE: emit keySpace(); break;
    case BTN_LEFT: emit buttonLeft(); break;
    case BTN_RIGHT: emit buttonRight(); break;
  }
}

void Input::timerExpired()
{
  delete m_installedTimer;
  m_installedTimer = nullptr;

  printf("Longpress: %s %s\n",
         libevdev_event_type_get_name(m_current_type),
         libevdev_event_code_get_name(m_current_type, m_current_code));
  if (m_current_type == EV_KEY) {
    switch (m_current_code) {
      case BTN_LEFT: emit holdButtonLeft(); break;
      case BTN_RIGHT: emit holdButtonRight(); break;
    }
  }
}

Input::~Input()
{
  libevdev_free(m_evdev);
}

QList<Input*> Input::getDevice(QString name)
{
  QList<Input*> ret;
  QDir eventdir("/dev/input", "event*",
                QDir::Unsorted, QDir::System | QDir::Readable);

  QStringList devices = eventdir.entryList();

  for (const QString &device : devices)
  {
    QFile dev(eventdir.filePath(device));
    if (dev.open(QFile::ReadOnly))
    {
      libevdev *evdev;
      int rc = libevdev_new_from_fd(dev.handle(), &evdev);
      if (rc == 0)
      {
        QString evname = libevdev_get_name(evdev);
        libevdev_free(evdev);
        if (name == evname)
          ret << new Input(dev.fileName());
      }
    }
  }
  return ret;
}

QStringList Input::listDeviceNames()
{
  QDir eventdir("/dev/input", "event*",
                QDir::Unsorted, QDir::System | QDir::Readable);

  QStringList devices = eventdir.entryList();
  QStringList ret = QStringList();
  
  for (const QString &device : devices)
  {
    QFile dev(eventdir.filePath(device));
    if (dev.open(QFile::ReadOnly))
    {
      libevdev *evdev;
      int rc = libevdev_new_from_fd(dev.handle(), &evdev);
      if (rc == 0)
      {
        QString evname = libevdev_get_name(evdev);
        libevdev_free(evdev);
        ret << evname;
      }
    }
  }
  ret.removeDuplicates();
  return ret;
}
