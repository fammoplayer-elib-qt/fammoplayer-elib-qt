#include "player.h"

#include <QDir>
#include <QMediaContent>
#include <QDebug>

Player::Player(QString cached_media_path, QObject *parent) :
  QObject(parent), m_cachedMediaPath(cached_media_path)
{
  connect(&m_player, &QMediaPlayer::stateChanged, this, &Player::onMediaPlayerStateChange);
  connect(&m_player, static_cast<void (QMediaPlayer::*)(QMediaPlayer::Error)>(&QMediaPlayer::error), this, &Player::onError);
  m_playList.setPlaybackMode(QMediaPlaylist::Sequential);
  m_player.setPlaylist(&m_playList);
}

void Player::setBook(QString book)
{
  QDir dir = QDir(m_cachedMediaPath).filePath(book);

  QStringList files = dir.entryList(QStringList() << "*.mp3" << "*.ogg" << "*.flac", QDir::Files | QDir::Readable, QDir::Name);

  QList<QMediaContent> tracks;
 
  for (const QString& file : files)
  {
    tracks << QMediaContent(QUrl::fromLocalFile(dir.filePath(file)));
  }
  if (!tracks.length()) {
    qWarning() << "directory" << dir << "contains no media files";
    return;
  }

  m_stopping_on_purpose = true;
  m_player.stop();
  m_playList.clear();

  if (!m_playList.addMedia(tracks))
    qWarning() << "Could not add media";
  else
    qDebug() << "Added media";

  for (int i = 0; i < m_playList.mediaCount(); i++)
  {
    const QMediaContent& media = m_playList.media(i);
    qDebug() << "playlist" << i << media.canonicalUrl();
  }

  m_playList.setCurrentIndex(0);

//  m_player.setPlaybackRate(20.0);
  m_stopping_on_purpose = false;
  m_player.play();
}

void Player::play()
{
  m_stopping_on_purpose = false;
  m_player.play();
}

void Player::pause()
{
  m_player.pause();
}

void Player::playPause()
{
  m_stopping_on_purpose = false;
  if (m_player.state() == QMediaPlayer::PlayingState)
    m_player.pause();
  else
    m_player.play();
}

void Player::nextTrack()
{
  m_player.playlist()->next();
}

void Player::prevTrack()
{
  if (m_playList.currentIndex() == 0)
  {
    emit prevBook();
  }
  else
  {
    m_playList.previous();
  }
}

void Player::changeVolume(int delta)
{
  m_player.setVolume(m_player.volume() + delta);
}

void Player::onMediaPlayerStateChange(QMediaPlayer::State state)
{
  qDebug() << "player state:" << state << m_player.playlist()->currentIndex() << m_player.mediaStatus();

  emit stateChanged(Player::State(state));

  if (!m_stopping_on_purpose && state == QMediaPlayer::StoppedState)
    emit nextBook();
}

void Player::onError(QMediaPlayer::Error error)
{
  qDebug() << "Error:" << error << ": " << m_player.errorString();
}
