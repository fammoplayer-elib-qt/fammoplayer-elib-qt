#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    updatePlaylist();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updatePlaylist()
{
  QSettings s;
  QStringList playlist = s.value("queue/list").toStringList();
  int index = s.value("queue/index").toInt();

  s.beginGroup("Library");
  QHash<QString, QVariant> bookSizes = s.value("BookSizes").toHash();
  s.endGroup();
  
  QListWidget *b = ui->bookList;
  b->clear();
  for (int i = 0; i < playlist.count(); i++) {
    QString book = playlist[i];
    if (bookSizes.contains(book)) {
      uint64_t size = bookSizes[book].toULongLong();
      book = book + " (" + QString::number(size >> 20) + " MB)";
    }
    b->addItem(book);
    b->item(i)->setFlags(Qt::ItemIsSelectable);
  }
  b->setCurrentRow(index);
}

void MainWindow::updatePlaying(Player::State state)
{
        ui->statusBar->showMessage(state == Player::StoppedState ? "Stoppad" :
                                   state == Player::PlayingState ? "Spelar" :
                                   "Pausad");
}
