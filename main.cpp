#include <QApplication>

#include "player.h"
#include "library.h"
#include "queue.h"
#include "input.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
  QCoreApplication::setOrganizationName("Fammo");
  QCoreApplication::setApplicationName("ElibPlayer-Qt");
  QCoreApplication::setApplicationVersion("2.0");

  QList<Input *>inputs = Input::getDevice("Device CY4672 PRoC LP RDK Bridge");
  if (inputs.empty()) {
    QStringList possible = Input::listDeviceNames();
    qDebug() << "Possible devices:" << possible;
  }

  QApplication a(argc, argv);

  Library l("/home/mhn/Downloads/elib-remote",
            "/home/mhn/Downloads/elib-local",
            100000000ULL);
  Player  p("/home/mhn/Downloads/elib-local");
  Queue   q;

  for (Input *i : inputs) {
    QObject::connect(i, &Input::keySpace, &p, &Player::playPause);
    QObject::connect(i, &Input::keyF, &p, &Player::nextTrack);
    QObject::connect(i, &Input::keyB, &p, &Player::prevTrack);
    QObject::connect(i, &Input::keyB, &p, &Player::prevTrack);
    QObject::connect(i, &Input::buttonLeft, &p, &Player::playPause);
    QObject::connect(i, &Input::buttonRight, &p, &Player::playPause);
    QObject::connect(i, &Input::holdButtonLeft, &p, &Player::nextTrack);
    QObject::connect(i, &Input::holdButtonRight, &p, &Player::prevTrack);
  }

  QObject::connect(&l, &Library::entriesCached,  &q, &Queue::onEntriesCached);
  QObject::connect(&l, &Library::entriesDeleted, &q, &Queue::onEntriesDeleted);
  QObject::connect(&l, &Library::indexUpdated,   &q, &Queue::onIndexUpdated);

  QObject::connect(&q, &Queue::pruneLocalBooks,  &l, &Library::pruneLocalRepository);
  QObject::connect(&q, &Queue::cacheBooks,       &l, &Library::cacheEntries);


  QObject::connect(&q, &Queue::bookChanged, &p, &Player::setBook);

  QObject::connect(&p, &Player::nextBook,   &q, &Queue::nextBook);
  QObject::connect(&p, &Player::prevBook,   &q, &Queue::prevBook);

//  for (const QString & deviceName : QStringList() << "SynPS/2 Synaptics TouchPad")
//  {
//    Device *d = Device::getDevice("SynPS/2 Synaptics TouchPad");
//  }

  l.start();
//  QTimer::singleShot(100, &l, SLOT(fetchIndex()));

  MainWindow w;
  QObject::connect(&w, &MainWindow::on_playPause_clicked,
                   &p, &Player::playPause);
  QObject::connect(&w, &MainWindow::on_prevTrack_clicked,
                   &p, &Player::prevTrack);
  QObject::connect(&w, &MainWindow::on_nextTrack_clicked,
                   &p, &Player::nextTrack);
  QObject::connect(&w, &MainWindow::on_nextBook_clicked,
                   &q, &Queue::nextBook);
  QObject::connect(&w, &MainWindow::on_prevBook_clicked,
                   &q, &Queue::prevBook);

  QObject::connect(&q, &Queue::bookChanged,
                   &w, &MainWindow::updatePlaylist);
  QObject::connect(&l, &Library::entriesCached,
                   &w, &MainWindow::updatePlaylist);
  QObject::connect(&p, &Player::stateChanged,
                   &w, &MainWindow::updatePlaying);

  w.show();

  return a.exec();
}
