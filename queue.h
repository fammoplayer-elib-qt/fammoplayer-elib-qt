#ifndef QUEUE_H
#define QUEUE_H

#include <QObject>
#include <QSet>
#include <QString>
#include <QStringList>

class Queue : public QObject
{
  Q_OBJECT
public:
  explicit Queue();

signals:
  void bookChanged(QString entry);
  void cacheBooks(QSet<QString> entries);
  void pruneLocalBooks(QStringList entries);

public slots:
  void nextBook();
  void prevBook();

  void onIndexUpdated(QSet<QString> index);
  void onEntriesCached(QSet<QString> entries);
  void onEntriesDeleted(QSet<QString> entries);

  void setCurrentBook(int index);

private:
  /* The set of all books at the remote server */
  QSet<QString> m_index;

  /* The set of all books cached locally */
  QSet<QString> m_cached;
};

#endif // QUEUE_H
