#include "library.h"

#include <QSettings>
#include <QDir>
#include <QDebug>

Library::Library(QString remote_media_path, QString cached_media_path,
                 uint64_t max_cached_bytes, QObject *parent) :
        QObject(parent), m_remoteMediaPath(remote_media_path),
        m_cachedMediaPath(cached_media_path),
        m_maxCachedBytes(max_cached_bytes)
{
  QSettings s;
  s.beginGroup("Library");
  m_bookSizes = s.value("BookSizes").toHash();
  m_totalSize = s.value("CachedSize").toULongLong();
  s.endGroup();

  connect(&m_rsync, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &Library::onRsyncFinished);
  connect(&m_find, static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished), this, &Library::onFetchFinished);
}

void Library::start()
{
  QSet<QString> cached_entries = m_bookSizes.keys().toSet();
  emit entriesCached(cached_entries);

  QTimer::singleShot(100, this, SLOT(fetchIndex()));

  m_timer.setInterval(24 * 60 * 60 * 1000);
  connect(&m_timer, &QTimer::timeout, this, &Library::fetchIndex);
  m_timer.start();
}

void Library::fetchIndex()
{
  if (m_find.state() == QProcess::NotRunning)
  {
    QStringList args;
    m_find.setProgram("rsync");
    args << "-mr";           /* omit empty directories */
    args << (m_remoteMediaPath + "/");
    args << "--include=.ISBN*";
    args << "--include=*/";  /* include all directories */
    args << "--exclude=*";   /* skip the rest of the regular files */
    m_find.setArguments(args);

    m_find.start();
  }
}

void Library::cacheEntries(QSet<QString> entries)
{
  for (const QString &book : entries)
    m_syncQueue << book;

  startSync();

  qDebug() << "Caching entries" << entries;
//  emit entriesCached(entries);
}

void Library::startSync()
{
  if (!m_syncQueue.isEmpty() && m_rsync.state() == QProcess::NotRunning)
  {
    QStringList args;
//    m_find.setProgram("ssh");
//    args << "find /home/mhn/Downloads/Liza* -name .ISBN\:* -printf '%P\n'";
    m_rsync.setProgram("rsync");
    args << "-rxvtCzl";
    args << "--files-from=-";
    args << m_remoteMediaPath + "/";
    args << m_cachedMediaPath + "/";
    m_rsync.setArguments(args);

    m_rsync.start();
    m_rsync.write(m_syncQueue.head().toUtf8());
    m_rsync.closeWriteChannel();
  }
}

void Library::onFetchFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
  Q_UNUSED(exitStatus);

  if (exitCode == 0)
  {
    QString out(m_find.readAllStandardOutput());
    QStringList lines = out.split("\n", QString::SkipEmptyParts);

    QSet<QString> books;

    for (const QString & line : lines)
    {
      /* the output is ls -l format, strip away stuff in beginning of line */

      /* TODO: the length depend on the locale, 46 characters should be
         stripped sometimes:
         -rw-r--r--     11,751,465 2014/08/06 23:04:31 Deckare & spänning/Dan Buthler/201209 - Återvändaren/Dan Buthler - Återvändaren - 022.mp3
       */
      QStringList info = line.mid(43).split("/.ISBN:");

      if (info.length() == 2)
        books << info[0];
    }
    emit indexUpdated(books);
  }
  else
  {
    qDebug() << "Failed to fetch index:" << m_find.readAllStandardError();
  }
}

void Library::onRsyncFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
  Q_UNUSED(exitStatus);

  if (exitCode == 0)
  {
    QString entry(m_syncQueue.dequeue());
    QString out(m_rsync.readAllStandardOutput());
    QStringList lines(out.split("\n", QString::SkipEmptyParts));
    QStringList words(lines.last().split(" ", QString::SkipEmptyParts));

    QString sizeString(words[3]);

    sizeString = sizeString.replace(',', "");

    qulonglong size = sizeString.toLongLong();

    qDebug() << "rsync:" << out << sizeString;

    m_bookSizes[entry] = size;
    m_totalSize += size;

    QSettings s;
    s.beginGroup("Library");
    s.setValue("BookSizes", m_bookSizes);
    s.setValue("CachedSize", m_totalSize);
    s.endGroup();

    emit entriesCached(QSet<QString>() << entry);
  }
  else
    qDebug() << "Syncing failed!";

  if (!m_syncQueue.isEmpty())
    startSync();
}

void Library::pruneLocalRepository(const QStringList & prio)
{
  QSet<QString> removed;

  int i = 0;
  while (m_totalSize > m_maxCachedBytes)
  {
    qDebug() << "Pruning local repo" << m_totalSize;
    for (; i < prio.size(); i++)
    {
      const QString & entry(prio[i]);
      if (m_bookSizes.contains(entry))
      {
        qDebug() << "Pruning book" << entry << i;

        QDir path(m_cachedMediaPath);
        path = path.filePath(entry);

        qDebug() << "Removing path:" << path;
        path.removeRecursively();

        removed << entry;
        m_totalSize -= m_bookSizes[entry].toULongLong();
        m_bookSizes.remove(entry);
        break;
      }
    }
    if (i >= prio.size())
      break;
  }

  QSettings s;
  s.beginGroup("Library");
  s.setValue("BookSizes", m_bookSizes);
  s.setValue("CachedSize", m_totalSize);
  s.endGroup();

  emit entriesDeleted(removed);
}
