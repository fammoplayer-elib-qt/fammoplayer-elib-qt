#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QMediaPlaylist>
#include <QMediaPlayer>

class Player : public QObject
{
  Q_OBJECT
public:
  enum State {StoppedState, PlayingState, PausedState};

  explicit Player(QString cached_media_path, QObject *parent = 0);

signals:
  void nextBook();
  void prevBook();
  void stateChanged(State state);

public slots:
  void setBook(QString book);
  void play();
  void pause();
  void playPause();
  void nextTrack();
  void prevTrack();
  void changeVolume(int delta);

private slots:
  void onMediaPlayerStateChange(QMediaPlayer::State state);
  void onError(QMediaPlayer::Error error);

private:
  QString m_cachedMediaPath;
  QMediaPlaylist m_playList;
  QMediaPlayer m_player;
  bool m_stopping_on_purpose;  /* true if the stop is triggered by us */
};

#endif // PLAYER_H
