#ifndef LIBRARY_H
#define LIBRARY_H

#include <QObject>
#include <QProcess>
#include <QString>
#include <QSet>
#include <QTimer>
#include <QQueue>
#include <QHash>
#include <QVariant>

class Library : public QObject
{
  Q_OBJECT
public:
  explicit Library(QString remote_media_path, QString cached_media_path,
                   uint64_t max_cached_bytes, QObject *parent = 0);
  void start();

signals:
  void indexUpdated(QSet<QString> index);
  void entriesCached(QSet<QString> entries);
  void entriesDeleted(QSet<QString> entries);

public slots:
  void fetchIndex();
  void cacheEntries(QSet<QString> entries);
  void pruneLocalRepository(const QStringList &prio);

private slots:
  void onFetchFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void onRsyncFinished(int exitCode, QProcess::ExitStatus exitStatus);
  void startSync();

private:

  QString m_remoteMediaPath, m_cachedMediaPath;
  QTimer m_timer;
  QProcess m_rsync, m_find;
  QQueue<QString> m_syncQueue;
  QHash<QString, QVariant> m_bookSizes;
  qulonglong m_maxCachedBytes;
  qulonglong m_totalSize = 0;
};

#endif // LIBRARY_H
