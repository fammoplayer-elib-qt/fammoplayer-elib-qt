#include "queue.h"

#include <QSettings>
#include <QDebug>

Queue::Queue()
{
}

static QStringList readQueue()
{
  return QSettings().value("queue/list").toStringList();
}
static int readCurrentIndex()
{
  return QSettings().value("queue/index").toInt();
}
static void writeCurrentIndex(int value)
{
  QSettings().setValue("queue/index", value);
}
static void writeQueue(QStringList queue)
{
  QSettings().setValue("queue/list", queue);
}

void Queue::nextBook()
{
  int currentIndex = readCurrentIndex();
  setCurrentBook(currentIndex + 1);
}

void Queue::prevBook()
{
  int currentIndex = readCurrentIndex();
  setCurrentBook(currentIndex - 1);
}

void Queue::setCurrentBook(int index)
{
  QStringList queue = readQueue();
  if (queue.size() == 0) {
    qWarning() << "setCurrentBook: No beek!\n";
    return;
  }
  index = qBound(0, index, queue.size() - 1);
  writeCurrentIndex(index);

  const QString & nextBook = queue[index];

  emit cacheBooks(QSet<QString>() << nextBook);
  emit cacheBooks(queue.mid(index - 1, 5).toSet() - m_cached);

  if (m_cached.contains(nextBook))
  {
    emit bookChanged(nextBook);
  } else {
    qDebug() << "Could not find book" << nextBook << "among cached ones";
  }
}

void Queue::onIndexUpdated(QSet<QString> index)
{
  QSet<QString> deleted = m_index - index;
  QSet<QString> deleted_and_not_cached = deleted - m_cached;
  QSet<QString> added = index - m_index;

  QStringList queue = readQueue();
  int currentIndex = readCurrentIndex();

  QString currentBook;
  if (queue.size() > 0)
    currentBook = queue[currentIndex];

  for (const QString& entry : deleted_and_not_cached)
    queue.removeOne(entry);
  for (const QString& entry : added)
    queue.push_back(entry);

  if (!currentBook.isNull())
    currentIndex = queue.indexOf(currentBook);

  writeQueue(queue);
  writeCurrentIndex(currentIndex);

  emit bookChanged(currentBook);

  emit cacheBooks(queue.mid(currentIndex, 5).toSet() - m_cached);

  m_index = index;
}

void Queue::onEntriesCached(QSet<QString> entries)
{
  m_cached.unite(entries);

  QStringList queue = readQueue();
  int currentIndex = readCurrentIndex();

  QStringList prio;
  if (currentIndex > 0)
    prio = queue.mid(0, currentIndex - 1);
  for (int i = queue.size() - 1; i >= 0; i--)
    if (m_cached.contains(queue[i]))
      prio << queue[i];

  emit pruneLocalBooks(prio);
}

void Queue::onEntriesDeleted(QSet<QString> entries)
{
  m_cached.subtract(entries);
}
