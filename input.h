#ifndef INPUT_H
#define INPUT_H

#include <QObject>
#include <QFile>
#include <QStringList>
#include <QTimer>
#include <libevdev/libevdev.h>

class Input : public QObject
{
  Q_OBJECT

public:
  ~Input();
  static QList<Input *> getDevice(QString name);
  static QStringList listDeviceNames();

private:
  explicit Input(QString path, QObject *parent = 0);

signals:
  void buttonLeft();
  void buttonRight();
  void holdButtonLeft();
  void holdButtonRight();
  void keyB();
  void keyF();
  void keyN();
  void keyP();
  void keySpace();

  void rawKeyCode(int type, int code, int value);

public slots:
  void handleRawKeyCode(int type, int code, int value);
  void timerExpired();

private:
  libevdev *m_evdev;
  QFile m_dev;

  /* NULL if no timer installed */
  QTimer *m_installedTimer;
  int m_current_type;
  int m_current_code;

public:
  void handleKeypress();
};

#endif // INPUT_H
